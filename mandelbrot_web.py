# https://trinket.io/processing
from processing import *

SQUARE_SIZE = 3
WIDTH = 400
HEIGHT = 300
ITERATIONS = 50
X_0, X_F = -2.1, 1.5
Y_0, Y_F = -1.7, 1.7

def setup():
    strokeWeight(SQUARE_SIZE)
    frameRate(20)
    size(WIDTH, HEIGHT)

def mandelbrot(x, y):
  MAX_RADIUS_2 = 2*2
  mod = 0
  z = (x,y)
  for _ in range(ITERATIONS):
    z = (z[0]*z[0]-z[1]*z[1]+x, 2*z[0]*z[1]+y)
    mod = z[0]*z[0]+z[1]*z[1]
    if mod > MAX_RADIUS_2:
      return mod

  return max(mod, MAX_RADIUS_2 - 0.01 < x*x + y*y < MAX_RADIUS_2)

def draw():
  background(100)
  H = HEIGHT // SQUARE_SIZE
  W = WIDTH // SQUARE_SIZE
  for y in range(H+1):
    for x in range(W+1):
      density = 255*(1-mandelbrot(X_0+(X_F-X_0)*x/W, Y_0+(Y_F-Y_0)*(H-y)/H))
      stroke(0, 0, density)
      rect(x*SQUARE_SIZE, y*SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE)
  textSize(20)
  stroke(255, 255, 255)
  text(str([X_0+(X_F-X_0)*mouse.x/WIDTH, Y_0+(Y_F-Y_0)*(HEIGHT-mouse.y)/HEIGHT, mandelbrot(-1.9,0)]), mouse.x, mouse.y)
run()